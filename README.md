# Pipelines Ignored in Merge Request (bug repro example)

This project shows that failing pipelines are ignored on merge requests, so long as the pipeline (for jobs with `only:refs:merge_requests`) passes.

See this project's [open merge request](https://gitlab.com/CauhxMilloy/pipelines-ignored-in-mr/merge_requests/1) for core of the example and further detail.

See [related issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/66367) that has been filed about this.
